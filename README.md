# Test Angular4 + MDB + Lazy Loading

## Como Angular5 es la version estable actual, para poder instalar angular 4 hay que establecer primero la version de npm y de angular-cli a usar:

- sudo npm install -g npm@3.10.10
- sudo npm install -g @angular/cli@1.4

## Para correr el proyecto:
- npm install
- ng serve --o

---

# Para instalar un proyecto nuevo
## Ahora si se puede crear el proyecto ya eligiendo utilizar rutas para lazy loading y scss para mdb

- ng new mdb-project --routing --style=scss

## Despues instalar el paquete de mdb y sus dependencias

- cd mdn-project
- npm i angular-bootstrap-md@4.3 --save
- npm i @agm/core@~1.0.0-beta.0 --save
- npm i chart.js@2.5.x --save
- npm i font-awesome@4.7.x --save
- npm i hammerjs@2.0.x --save

### Configurar MDB segun:
https://mdbootstrap.com/getting-started/5min-quickstart/


### Generar modulos para lazy loading segun:
https://angular.io/guide/lazy-loading-ngmodules
